#!/bin/bash

# Starting coordinates
latitude=11.186786763417071
longitude=77.31191969211241

# Function to generate a small random change in coordinates
generate_random_coordinate() {
  # Small change values (approximately 0.01 degrees corresponds to ~1 km)
  lat_change=$(awk 'BEGIN{srand(); print -0.01 + (rand() * (0.05 - (-0.01)))}')
  lon_change=$(awk 'BEGIN{srand(); print -0.01 + (rand() * (0.1 - (-0.01)))}')

  # Update coordinates
  latitude=$(awk -v lat=$latitude -v change=$lat_change 'BEGIN{print lat + change}')
  longitude=$(awk -v lon=$longitude -v change=$lon_change 'BEGIN{print lon + change}')
}

while true; do
  # Generate random coordinates
  generate_random_coordinate

  # Prepare JSON data with double quotes for proper formatting
  json_data="{\"latitude\":$latitude, \"longitude\":$longitude}"

  echo "Publishing random coordinates: latitude=$latitude, longitude=$longitude"

  # Publish random coordinates to a topic
  mosquitto_pub -h localhost -t random_coordinates_topic -m "$json_data"

  # Wait for 5 seconds before publishing the next message
  sleep 5
done
