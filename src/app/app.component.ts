import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { GoogleMap, MapMarker, MapPolyline } from '@angular/google-maps';
import { RouterOutlet } from '@angular/router';
import { IMqttMessage, MqttService } from 'ngx-mqtt';
import { map, pipe } from 'rxjs';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, GoogleMap, MapMarker, MapPolyline, CommonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  title = 'angular-learning';
  center: google.maps.LatLngLiteral = { lat: 11.2283, lng: 77.3963 };
  zoom = 9;
  markerOptions: google.maps.MarkerOptions = { draggable: false };
  coordinatesList: google.maps.LatLngLiteral[] = [];
  constructor(private mqttService: MqttService) {}
  ngOnInit() {
    this.mqttService
      .observe('#')
      .pipe(
        map((data: IMqttMessage) => {
          const temp: ImqttCoordinate = JSON.parse(data.payload.toString());
          return temp;
        })
      )
      .subscribe((data: ImqttCoordinate) => {
        this.coordinatesList = this.coordinatesList.concat({
          lat: data.latitude,
          lng: data.longitude,
        });
        console.log(this.coordinatesList);
      });
  }
}
interface ImqttCoordinate {
  latitude: number;
  longitude: number;
}
